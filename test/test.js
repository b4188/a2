const { factorial, oddEven, div_check } = require("../src/util.js");
const { expect, assert } = require("chai");

describe("text_fun_factorials", () => {
	it("test_fun_factorial_5!_is_120", () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	}) 
	it("test_fun_factorial_1!_is_1", () => {
		const product = factorial(1);
		expect(product).to.equal(1);
	});
	//0!
	it("test_fun_factorial_0!_is_1", () => {
		const product = factorial(0);
		expect(product).to.equal(1);
	});
	//4!
	it("test_fun_factorial_4!_is_24", () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	});
	//10!
	it("test_fun_factorial_10!_is_3628800", () => {
		const product = factorial(10);
		expect(product).to.equal(3628800);
	});

	it("test_fun_factorial_-1!_is_undefined", () => {
		const product = factorial(-1);
		expect(product).to.equal(undefined);
	});

	it('Test factorial is non-numeric', () => {
		const product = factorial('one');
		expect(product).to.equal(undefined);
	})
});


describe("text_num", () => {
	it("test_2_is_even", () => {
		const num = oddEven(2);
		expect(num).to.equal(0); //uses chainable language to construct assertions
	}) 


	it("test_7_is_odd", () => {
		const num = oddEven(7);
		assert.equal(num,1); //uses assert-dot notation that node.js has
	});

	it('Test oddEven is non-numeric', () => {
		const num = oddEven('one');
		expect(num).to.equal(undefined);
	})
})

describe("test divisiblility", () => {
	it("test_105_is_divisible_by_5", () => {
		const num = div_check(105);
		expect(num).to.equal(true);
	}) 
	it("test_14_is_divisible_by_7", () => {
		const num = div_check(7);
		assert.equal(num,true);
	});
		it("test_0_is_divisible_by_5_or_7", () => {
		const num = div_check(0);
		assert.equal(num,true);
	});
		it("test_22_is_divisible_by_5_or_7", () => {
		const num = div_check(22);
		assert.equal(num,false);
	});

		it('Test div_check is non-numeric', () => {
		const num = div_check('one');
		expect(num).to.equal(undefined);
	})


})