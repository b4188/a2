function factorial(n){
	if(typeof n !== "number") return undefined;
	if(n<0) return undefined;
	if(n==0) return 1;
	if(n==1) return 1;
	return n * factorial(n-1);
}



function oddEven(y){
	if(typeof y !== "number") return undefined;
	if(y === 0) return true
		return (y % 2);
}



function div_check(y){
	if(typeof y !== "number") return undefined;
	if ((y % 5) === 0 || (y % 7) === 0) {
		return true
	} else {
		return false
	}
}

const names = {
	"Boba": {
		"name": "Boba Fett",
		"age": 50,
		"alias": "bf"
	},
	"Anakin": {
		"name": "Anakin Skywalker",
		"age": 65,
		"alias": "as"
	}
}

module.exports = {
	factorial:factorial,
	oddEven:oddEven,
	div_check:div_check
}